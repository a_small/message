package com.message.ui.msg.service;

import com.message.admin.msg.pojo.MsgInfo;
import com.system.handle.model.ResponseFrame;

public interface UiMsgInfoService {

	public ResponseFrame pageQuery(MsgInfo msgInfo);

	public MsgInfo get(String msgId);

}
